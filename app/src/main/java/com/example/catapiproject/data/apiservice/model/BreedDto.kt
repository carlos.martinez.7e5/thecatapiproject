package com.example.catapiproject.data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


//Habría que revisar cual es nullable y cual no
@Serializable
data class BreedDto (
    //@SerialName el nombre literal, nombre de la variable el que yo quiera (mejor si es el mismo)

    @SerialName("weight") val weight : CatWeightDto,
    @SerialName("id") val id : String,
    @SerialName("name") val name : String,
    @SerialName("cfa_url") val cfa_url : String? = null,
    @SerialName("vetstreet_url") val vetstreet_url : String? = null,
    @SerialName("vcahospitals_url") val vcahospitals_url : String? = null,

    //temperament parece un String[], pero todo_ va junto dentro de unas mismas comillas
    @SerialName("temperament") val temperament : String,

    @SerialName("origin") val origin : String,
    @SerialName("country_codes") val country_codes : String,
    @SerialName("country_code") val country_code : String,
    @SerialName("description") val description : String,
    @SerialName("life_span") val life_span : String,
    @SerialName("indoor") val indoor : Int,
    @SerialName("lap") val lap : Int? = null,

    //Puede ser nulo (en el ejemplo que he visto) https://api.thecatapi.com/v1/images/0XYvRd7oD
    @SerialName("alt_names") val alt_names : String? = null,

    @SerialName("adaptability") val adaptability : Int,
    @SerialName("affection_level") val affection_level : Int,
    @SerialName("child_friendly") val child_friendly : Int,
    @SerialName("dog_friendly") val dog_friendly : Int,
    @SerialName("energy_level") val energy_level : Int,
    @SerialName("grooming") val grooming : Int,
    @SerialName("health_issues") val health_issues : Int,
    @SerialName("intelligence") val intelligence : Int,
    @SerialName("shedding_level") val shedding_level : Int,
    @SerialName("social_needs") val social_needs : Int,
    @SerialName("stranger_friendly") val stranger_friendly : Int,
    @SerialName("vocalisation") val vocalisation : Int,
    @SerialName("experimental") val experimental : Int,
    @SerialName("hairless") val hairless : Int,
    @SerialName("natural") val natural : Int,
    @SerialName("rare") val rare : Int,
    @SerialName("rex") val rex : Int,
    @SerialName("suppressed_tail") val suppressed_tail : Int,
    @SerialName("short_legs") val short_legs : Int,
    @SerialName("wikipedia_url") val wikipedia_url : String? = null,
    @SerialName("hypoallergenic") val hypoallergenic : Int,
    @SerialName("reference_image_id") val reference_image_id : String? = null
    )