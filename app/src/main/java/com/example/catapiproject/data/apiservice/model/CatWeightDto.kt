package com.example.catapiproject.data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CatWeightDto (
    @SerialName("imperial") val imperial : String,
    @SerialName("metric") val metric : String,
)
