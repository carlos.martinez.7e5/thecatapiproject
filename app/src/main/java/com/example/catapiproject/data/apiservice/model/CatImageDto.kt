package com.example.catapiproject.data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CatImageDto (
    @SerialName("id") val id : String, //está entre comillas, es string
    @SerialName("url") val url : String?,
    @SerialName("width") val width : Int,
    @SerialName("height") val height : Int,
)