package com.example.catapiproject.data.apiservice

import com.example.catapiproject.data.apiservice.model.BreedDto
import com.example.catapiproject.data.apiservice.model.CatImageDto
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query

private const val BASE_URL = "https://api.thecatapi.com"

private val json = Json { ignoreUnknownKeys = true }

//Builder
@OptIn(ExperimentalSerializationApi::class)
private val retrofit = Retrofit.Builder()
    .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
    .baseUrl(BASE_URL)
    .build()

object CatApi {

    //lazy, hasta que no se use no se crea
    val retrofitService: CatApiService by lazy {
        retrofit.create(CatApiService::class.java)
    }
}

interface CatApiService{

    // Las funciones que llaman a la API

    //Sólo te devuelve una imagen del id que sea
    @GET("./v1/images/search") //la parte variable, que se le suma a BASE_URL
    suspend fun getCatImage(@Query("breed_id") id: String) : List<CatImageDto>

    @GET("./v1/breeds")
    suspend fun getCatBreeds() : List<BreedDto>
}