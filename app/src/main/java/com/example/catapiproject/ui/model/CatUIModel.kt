package com.example.catapiproject.ui.model

import android.os.Parcelable
import com.example.catapiproject.data.apiservice.model.CatWeightDto
import kotlinx.android.parcel.Parcelize
import kotlinx.serialization.SerialName

@Parcelize
data class CatUIModel (
    val name : String,
    val temperament : String,
    val country_code : String,
    val description : String,
    val imageUrl : String?,
    val wikiUrl : String?
    ): Parcelable

@Parcelize
data class CatWeightUiModel (
    val imperial : String,
    val metric : String,
): Parcelable
