package com.example.catapiproject.ui.model.mappers

import com.example.catapiproject.data.apiservice.model.BreedDto
import com.example.catapiproject.data.apiservice.model.CatImageDto
import com.example.catapiproject.ui.model.CatUIModel
import com.example.catapiproject.ui.model.CatWeightUiModel

class CatUIModelMapper {

    fun map(breedList: List<BreedDto>, imageList: List<CatImageDto>): List<CatUIModel> {

//        val catList = mutableListOf<CatUIModel>()
//
//        //Por como esta hecho, 100% habrá el mismo numero de fotos que de razas
//        //Rellena los datos de cada gato usando los datos de las dos listas
//        for (i in 1..breedList.count()){
//            catList.add(
//                CatUIModel(
//                    weight = breedList[i].weight,
//                    imageUrl = imageList[i].url,
//                    id = breedList[i].id,
//                    name = breedList[i].name,
//                    cfa_url = breedList[i].cfa_url,
//                    vetstreet_url = breedList[i].vetstreet_url,
//                    vcahospitals_url = breedList[i].vcahospitals_url,
//                    temperament = breedList[i].temperament,
//                    origin = breedList[i].origin,
//                    country_code = breedList[i].country_code,
//                    country_codes = breedList[i].country_codes,
//                    description = breedList[i].description,
//                )
//            )
//        }

        return mutableListOf<CatUIModel>().apply {
            breedList.forEachIndexed { index, breed ->
                add (CatUIModel(
                    imageUrl = imageList[index].url,
                    name = breed.name,
                    temperament = breed.temperament,
                    country_code = breed.country_code,
                    description = breed.description,
                    wikiUrl = breed.wikipedia_url
                ))
            }
        }
    }
}