package com.example.catapiproject.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.example.catapiproject.ui.model.CatUIModel

@Composable
fun DetailedCat(cat : CatUIModel) {

    //Para hacer que se pueda clickar el enlace
    val uriHandler = LocalUriHandler.current

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState()),
    ) {

        Image(
            painter = rememberAsyncImagePainter(cat.imageUrl),
            contentDescription = "Cat image",
            modifier = Modifier
                .padding(10.dp)
                .size(350.dp)
            )

        Text(
            text = cat.name,
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.ExtraBold,
            style = MaterialTheme.typography.h5,
            modifier = Modifier.padding(16.dp)
        )

        Text(
            text = cat.description,
            style = MaterialTheme.typography.body2,
            modifier = Modifier.padding(16.dp)
        )

        Text(
            text = cat.country_code,
            style = MaterialTheme.typography.body2,
            modifier = Modifier.padding(16.dp)
        )

        Text(
            text = cat.temperament,
            style = MaterialTheme.typography.body2,
            modifier = Modifier.padding(16.dp)
        )

        if (cat.wikiUrl != null) {
            Text(
                text = cat.wikiUrl,
                color = Color.Blue,
                style = MaterialTheme.typography.body2,
                modifier = Modifier
                    .padding(16.dp)
                    .clickable { uriHandler.openUri(cat.wikiUrl) },
            )
        }
    }
}
