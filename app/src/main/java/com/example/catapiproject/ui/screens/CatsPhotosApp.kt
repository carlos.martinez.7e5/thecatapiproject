package com.example.catapiproject.ui.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel

@SuppressLint("StateFlowValueCalledInComposition")
@Composable
fun CatsApp(){
    //Para que me deje llamar al ViewModel hay que hacerlos desde una composable
    val catsList : CatsViewModel = viewModel()
    val uiState = catsList.catsUiState.collectAsState()
    CatList(catList = uiState.value)

    if(uiState.value != null && uiState.value!!.isNotEmpty()) {
        CatList(catList = uiState.value)
    } else CircularLoad()
}

@Composable
fun CircularLoad() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator(
            modifier = Modifier.size(150.dp))
    }
}
