package com.example.catapiproject.ui.screens

import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import com.example.catapiproject.MainActivity
import com.example.catapiproject.R

var users = listOf("carlos", "estibaliz", "admin")
var passwords = listOf("1234", "4321", "admin")


@Composable
fun LoginScreen() {

    var username by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }

    var errorCount: Int by remember { mutableStateOf(0) }
    var dialogOpen by remember { mutableStateOf(false) }

    Column(

        modifier = Modifier
            .fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {

        Image(
            painter = painterResource(id = R.drawable.portada),
            contentDescription = "La portada de la aplicación"
        )

        Spacer(modifier = Modifier.height(10.dp))

        val userMaxLenght = 14

        //Username
        OutlinedTextField(
            value = username,
            onValueChange = {
                if (it.length <= userMaxLenght) {
                    username = it
                }
            },
            label = { Text("Username") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 50.dp, end = 50.dp, top = 10.dp, bottom = 10.dp),
            maxLines = 1 //Para que no se puedan escribir lineas debajo
        )

        Spacer(modifier = Modifier.height(16.dp))

        val passwordMaxLength = 12

        //Password
        OutlinedTextField(
            value = password,
            onValueChange = {
                if (it.length <= passwordMaxLength) {
                    password = it
                }
            },
            label = { Text(text = "Password") },
            visualTransformation = PasswordVisualTransformation(), //para que no se vea la contraseña
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 50.dp, end = 50.dp, top = 10.dp, bottom = 10.dp),

            )

        Spacer(modifier = Modifier.height(16.dp))

        val context = LocalContext.current

        Button(
            onClick = {
                if (validateUser(username)) {
                    val i = getUserPosition(username)
                    if (password == passwords[i]) {
                        val intent = Intent(context, MainActivity::class.java)
                        context.startActivity(intent)
                    } else {
                        incorrectToast(context)
                        errorCount++
                    }
                } else { incorrectToast(context) }
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 50.dp, end = 50.dp, top = 10.dp, bottom = 10.dp),
        ) {
            Text("Sign In")
        }

        if (errorCount == 3) {
            dialogOpen = true
        }

        if (dialogOpen) {
            AlertDialog(
                onDismissRequest = { },
                buttons = {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 24.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Button(
                            onClick = {
                                dialogOpen = false
                                errorCount = 0
                            }
                        ) {
                            Text(text = "Ok")
                        }
                    }
                },
                title = {
                    Text(text = "Blocked Account")
                },
                text = {
                    Text(text = "Too many failed attempts. Your account is blocked, contact with admins to recover it")
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(32.dp),
                shape = RoundedCornerShape(5.dp),
                backgroundColor = Color.White
            )
        }
    }
}

fun incorrectToast(context: Context) {
    val text = "Incorrect username or password "
    val duration = Toast.LENGTH_SHORT
    val toast = Toast.makeText(context, text, duration)
    toast.show()
}

fun validateUser(userName: String): Boolean {
    users.forEach {
        if (it == userName) return true
    }
    return false
}

fun getUserPosition(userName: String): Int {
    users.forEachIndexed { index, s ->
        if (s == userName) return index
    }
    return 0
}