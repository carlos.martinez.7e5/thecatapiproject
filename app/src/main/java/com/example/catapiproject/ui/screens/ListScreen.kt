package com.example.catapiproject.ui.screens

import android.content.Intent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.catapiproject.DetailActivity
import com.example.catapiproject.ui.model.CatUIModel

@Composable
fun CatList(catList: List<CatUIModel>?) {

    if (catList != null) {

        Column {

            FilterByCountry()

            LazyColumn {
                this.items(
                    items = catList,
                    itemContent = { item ->
                        CatCard(cat = item)
                    }
                )
            }
        }

    }
}

@Composable
fun CatCard(cat: CatUIModel) {
    Card {
        Column(
            modifier = Modifier
                .animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioMediumBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)

            ) {
                Image(
                    painter = rememberAsyncImagePainter(cat.imageUrl),
                    contentDescription = "Imagen del gato",
                    modifier = Modifier
                        .size(64.dp)
                        .padding(8.dp)
                        .clip(RoundedCornerShape(50.dp)),
                    contentScale = ContentScale.Crop
                )
                Spacer(Modifier.padding(8.dp))
                Text(
                    text = cat.name,
                    fontSize = 13.sp,
                    modifier = Modifier.weight(1f)
                )
                Text(
                    text = cat.country_code,
                    fontSize = 13.sp,
                    modifier = Modifier.weight(1f)
                )

                val context = LocalContext.current
                TextButton(
                    onClick = {
                        //Intent a la Detail Activity
                        val intent = Intent(context, DetailActivity::class.java)

                        intent.putExtra("name", cat.name)
                        intent.putExtra("imageUrl", cat.imageUrl)
                        intent.putExtra("description", cat.description)
                        intent.putExtra("country", cat.country_code)
                        intent.putExtra("temperament", cat.temperament)
                        intent.putExtra("wikiUrl", cat.wikiUrl)
                        intent.putExtra("countryCode", cat.country_code)

                        context.startActivity(intent)
                    }) {
                    Text(text = "About me...")
                }
            }
        }
    }
}

@Composable
fun FilterByCountry() {

    var country by remember { mutableStateOf("") } //El valor del Textfield de Country ID
    val vModel : CatsViewModel = viewModel() //Para llamar las funciones de filtrado

    Row(modifier = Modifier.padding(all = 5.dp)) {

        OutlinedTextField(
            value = country,
            onValueChange = {
                if (it.length <= 2) {
                    country = it
                }
            },
        )

        TextButton(
            onClick = {
                if (country == "") vModel.restoreAllCats()
                else vModel.getCatsByCountryID(country)
            },
            modifier = Modifier.padding(all = 5.dp)
        ) {
            Text(text = "Filter by country ID")
        }
    }
}