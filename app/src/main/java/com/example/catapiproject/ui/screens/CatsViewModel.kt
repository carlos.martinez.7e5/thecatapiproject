package com.example.catapiproject.ui.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.catapiproject.data.apiservice.CatApi
import com.example.catapiproject.ui.model.CatUIModel
import com.example.catapiproject.ui.model.mappers.CatUIModelMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CatsViewModel : ViewModel() {
    //El que se modifican los datos
    private var _catsUiState = MutableStateFlow<List<CatUIModel>?>(null)
    //Lista auxiliar, se guarda todos los gatos de la primera consulta y no se modifica
    private val auxilarAllCatsList = MutableStateFlow<List<CatUIModel>?>(null)

    //El que la Activity puede consultar pero no modificar
    val catsUiState: StateFlow<List<CatUIModel>?> = _catsUiState.asStateFlow()

    //Instancia de la clase que permite mapear las consultas
    private var imgMapper = CatUIModelMapper()

    //lo que hará según se crea
    init {
        getCats()
    }

    //Función para obtener todos los gatos
    private fun getCats() {
        viewModelScope.launch {

            //La lista de las razas de gatos
            val catBreeds = CatApi.retrofitService.getCatBreeds()

            //Una lista vacía de imagenes
            val catImages = catBreeds.flatMap {

                //Hay algunos que no tienen foto, asi que si no tiene foto le pone por defecto la del gato abys
                CatApi.retrofitService.getCatImage(it.id).ifEmpty {
                    CatApi.retrofitService.getCatImage("abys")
                }
            }
            _catsUiState.value = imgMapper.map(catBreeds, catImages)
            auxilarAllCatsList.value = _catsUiState.value
        }
    }

    //Función para obtener los gatos filtrados por countryID
     fun getCatsByCountryID(countryID : String) {

        val idFilteredCats = mutableListOf<CatUIModel>()

        auxilarAllCatsList.value!!.forEach {
            if (it.country_code == countryID) idFilteredCats.add(it)
        }

        _catsUiState.value = idFilteredCats
    }

    //Función para obtener todos los gatos (después de haber filtrado)
    fun restoreAllCats(){
         _catsUiState.value = auxilarAllCatsList.value
    }
}