package com.example.catapiproject

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.example.catapiproject.ui.Theme
import com.example.catapiproject.ui.screens.LoginScreen

class LoginActivity: ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Theme {
                LoginScreen()
            }
        }
    }
}