package com.example.catapiproject

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.example.catapiproject.ui.Theme
import com.example.catapiproject.ui.model.CatUIModel
import com.example.catapiproject.ui.screens.DetailedCat

class DetailActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        //Lo suyo sería hacerlo con parcelable, pero aparentemente en SDK 33 no va

        val catName : String?
        val catImage : String?
        val catDescription : String?
        val catTemperament : String?
        val catWikiUrl : String?
        val catCountryCode : String?
        val cat : CatUIModel
        val bundle: Bundle? = intent.extras

        bundle.apply {
            catName = this?.getString("name")
            catImage = this?.getString("imageUrl")
            catDescription = this?.getString("description")
            catTemperament = this?.getString("temperament")
            catWikiUrl = this?.getString("wikiUrl")
            catCountryCode = this?.getString("countryCode")

            cat = CatUIModel(
                name = catName!!,
                imageUrl = catImage!!,
                description = catDescription!!,
                temperament = catTemperament!!,
                wikiUrl = catWikiUrl!!,
                country_code = catCountryCode!!
            )
        }

        super.onCreate(savedInstanceState)

        setContent {
            Theme {
                DetailedCat(cat = cat)
            }
        }
    }
}