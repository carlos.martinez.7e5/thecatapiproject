# The Cat API Project

_Un proyecto para aprender a hacer llamadas a API desde Android Studio_

## Extras ##

#### Login ####
Pantalla mockeada para hacer login con unos usuarios y contraseñas prehechos localmente:

* carlos --> 1234
* estibaliz --> 4321
* admin --> admin

En caso de poner una mala combinación de usuario / contraseña, saltará un toast avisando del error. Al tener 3 errores, aparecerá un AlertDialogue anunciando que la cuenta ha sido suspendida (no hace nada, pero simula el sistema). Al poner una combinación correcta, pasa a la siguiente pantalla.

#### Lista ####
Al "iniciar sesión" comenzará la llamada a la API. Algunas veces falla, devolviendo a la pantalla anterior, pero se vuelve a intentar y funciona correctamente.
Se pueden filtrar los gatos según su country_code utilizando el TextBox para escribir el código y el botón para filtrar.
Para volver a ver todos los gatos, hay que dejar el campo vacío y filtrar.

#### Detalle ####
En la pantalla de detalle del gato, se puede clickar al enlace, y te lleva a la página.